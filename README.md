Maven in 5 minutes using Docker and Jenkins
|| || || || \\ \\ \\
\/ \/ \/ \/  \/ \/ \_____________________________________________________
https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html


1. For first you need to have your Docker instaled on your OS.
2. Pull Jenkins image and run it.
3. Add Maven installer on Jenkins:
4. Manage Jenkins -> Global Tool Configuration -> 
   Maven installations -> Choose your version -> Add Name of installer (preferred): Maven <Version>
5. On Jenkins main page add new item -> pipline -> name (e.g Maven_in_5_minutes)
6. Check file in repo: Maven_5. Copy/Paste script from the file to Pipline on Jenkins and Save.
   As you can see, names of stages comes from "Maven in 5 minutes" procedurs".
7. Run Build of created job.
8. Check logs and test result. 

On the end of build workspace will be cleaned. Reasons: Witout clearaing workspace, mvn:generate will fail becouse 
project with the same name alredy exist. 
Check file in "others" dir. Pipline in the file provide possiblity to not clear workspace, each created app get name
with build number in it. 